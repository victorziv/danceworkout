# from flask import current_app as cap
from dba import QueryTestcase
from . import db
# =============================


class Testcase:

    query = QueryTestcase(db)
    table = 'testcase'
    # ____________________________

    def __init__(self, name):
        self.name = name
    # ____________________________

    @classmethod
    def add(cls, attrs):
        rowid = cls.query.create(table='testcase', attrs=attrs)
        return rowid
    # ____________________________

    @classmethod
    def clear_table(cls):
        status = cls.query.remove_all_records()
        return status
    # ____________________________

    @classmethod
    def get_checksum(cls, name):
        return cls.query.read_checksum(name)
    # ____________________________

    @classmethod
    def get_all(cls, args):
        testcases = cls.query.read(**args)
        print("Get all return: {}".format(testcases))
        return testcases
    # ____________________________

    @classmethod
    def get_total(cls):
        return cls.query.read_total()
    # ____________________________

    @classmethod
    def remove(cls, name):
        attrs = {'name': name}
        rowid = cls.query.delete(table=cls.table, attrs=attrs)
        return rowid
    # ____________________________

    def update(self, case):
        self.query.update(
            table=self.table, key_name='name', key_value=case['name'], items=case)
