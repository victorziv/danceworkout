from flask import current_app as cap
from dba import QueryMachines
from . import db
# =============================


class Machine:

    query = QueryMachines(db)
    # ____________________________

    def __init__(self):
        pass
    # ____________________________

    @classmethod
    def add(cls, attrs):
        rowid = cls.query.create(table='machines', attrs=attrs)
        return rowid
    # ____________________________

    @classmethod
    def clear_table(cls):
        status = cls.query.remove_all_records()
        return status
    # ____________________________

    @classmethod
    def get_all(cls, args):
        machines = cls.query.read(**args)
        cap.logger.debug("Get all return: {}".format(machines))
        return machines
    # ____________________________

    @classmethod
    def get_total(cls):
        return cls.query.read_total()
