from dba import QueryCustomer
from . import db
from .base import BaseModel
# =============================


class Customer(BaseModel):

    query = QueryCustomer(db)
    # ____________________________

    @classmethod
    def add(cls, attrs):
        rowid = cls.query.create(table='customers', attrs=attrs)
        return rowid
    # ____________________________

    @classmethod
    def clear_table(cls):
        status = cls.query.remove_all_records()
        return status
    # ____________________________

    @classmethod
    def get_all(cls):
        machines = cls.query.read()
        return machines
    # ____________________________
