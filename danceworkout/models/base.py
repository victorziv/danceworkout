from flask import abort
from flask import current_app as cap
from psycopg2 import IntegrityError
# =========================================


class Permission:
    FOLLOW = 0x01               # 0b00000001
    COMMENT = 0x02              # 0b00000010
    WRITE_ARTICLES = 0x04       # 0b00000100
    MODERATE_COMMENTS = 0x08    # 0b00001000
    ADMINISTER = 0x80           # 0b10000000
# =========================================


class BaseModel:
    # ____________________________

    def __init__(self, attrs=None):
        if attrs is not None and type(attrs).__name__ == 'dict':
            self.__dict__.update(attrs)
    # ____________________________

    @classmethod
    def fetch_all(cls):
        model_list = cls.query.read()
        return [cls(attrs=m) for m in model_list]
    # __________________________________

    @classmethod
    def get_by_field(cls, name, value):
        kwargs = {name: value}
        modeld = cls.query.read_one_by_field(**kwargs)
        if modeld is None:
            return

        model_instance = cls(attrs=dict(modeld))
        return model_instance
    # __________________________________

    @classmethod
    def get_by_field_or_404(cls, name, value):
        kwargs = {name: value}
        modeld = cls.query.read_one_by_field(**kwargs)
        if modeld is None:
            abort(404)

        model_instance = cls(attrs=dict(modeld))
        return model_instance
    # __________________________________

    @classmethod
    def clear_table(cls):
        cls.query.remove_all_records()
    # __________________________________

    def create(self, table, attrs):

        query_template = """
            INSERT INTO %(table)s ({})
            VALUES ({})
            RETURNING id
        """
        fields = ', '.join(attrs.keys())
        cap.logger.debug("Fields: {}".format(fields))
        values_placeholders = ', '.join(['%s' for v in attrs.values()])
        query = query_template.format(fields, values_placeholders)
        cap.logger.debug("query: {}".format(query))
        cap.logger.debug("values: {}".format(attrs.values()))
        params = tuple(attrs.values())

        print("Query: {}".format(self.db.cursor.mogrify(query, params)))

        try:
            self.db.cursor.execute(query, params)
            self.db.conn.commit()
            fetch = self.db.cursor.fetchone()
            return fetch['id']
        except IntegrityError:
            self.db.conn.rollback()
        except Exception:
            raise
    # ____________________________
# ===========================
