#!/usr/bin/env python


def create_table_roles(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS roles (
            id serial PRIMARY KEY,
            name VARCHAR(64) UNIQUE,
            isdefault BOOLEAN DEFAULT FALSE,
            permissions INTEGER
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def create_table_users(conn):

    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS users (
            id serial PRIMARY KEY,
            social_id VARCHAR(64) UNIQUE,
            email VARCHAR(64) UNIQUE,
            username VARCHAR(128),
            password_hash VARCHAR(128),
            role_id INTEGER REFERENCES roles(id)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def create_table_machines(conn):

    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS machines (
            id serial PRIMARY KEY,
            name VARCHAR(32) UNIQUE,
            machineid INTEGER UNIQUE,
            ticket VARCHAR(32) UNIQUE,
            owner VARCHAR(128)
        );
    """
    params = []

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_machines(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS machines;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_roles(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS roles;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_users(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS users;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_roles(conn)
    create_table_users(conn)
    create_table_machines(conn)
# _______________________________


def downgrade(conn):
    drop_table_users(conn)
    drop_table_roles(conn)
    drop_table_machines(conn)
