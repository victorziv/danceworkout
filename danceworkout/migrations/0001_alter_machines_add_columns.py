#!/usr/bin/env python


def add_columns_table_machines(conn):

    cursor = conn.cursor()

    query = """
        ALTER TABLE IF EXISTS machines
        ADD COLUMN IF NOT EXISTS iboxmodel VARCHAR(16),
        ADD COLUMN IF NOT EXISTS
            created TIMESTAMP WITHOUT TIME ZONE DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')
    """
    params = []

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def remove_columns_table_machines(conn):
    cursor = conn.cursor()

    query = """
        ALTER TABLE IF EXISTS machines
        DROP COLUMN IF EXISTS iboxmodel,
        DROP COLUMN IF EXISTS created
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    add_columns_table_machines(conn)
# _______________________________


def downgrade(conn):
    remove_columns_table_machines(conn)
