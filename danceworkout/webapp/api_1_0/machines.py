from flask import jsonify, request
from flask import current_app as cap
from . import api
from models import Machine
from webapp.lib import reporthelper
# ______________________________________

"""
It is a common practice to define
URLs that represent collections of resources with a trailing slash, as this gives them a
"folder" representation.
"""


@api.route('/machines/', methods=['GET'])
def get_machines():
    cap.logger.debug("Get machines args: {}".format(request.args))

    current_page = int(request.args['page'])
    rows_limit = int(request.args['rows'])
    machines_total = Machine.get_total()
    cap.logger.debug("Machines total: %s", machines_total)
    total_pages, offset = reporthelper.evaluate_page_data(machines_total, current_page, rows_limit)
    cap.logger.debug("Total pages: %s", total_pages)
    cap.logger.debug("Offset: %s", offset)

    args = {
        'offset': offset,
        'limit': rows_limit
    }
    machines = Machine.get_all(args)
    cap.logger.debug("Machines: {}".format(machines))

    response_data = {
        "totalpages": total_pages,
        "totalrecords": machines_total,
        "currpage": current_page,
        "machines": [dict(m) for m in machines]
    }

    return jsonify(response_data)
# ______________________________________


@api.route('/machines/', methods=['POST'])
def add_machine():
    machine = request.get_json()['machine']
    machineid = Machine.add(attrs=machine)
    return jsonify({'machineid': machineid})
# ______________________________________


@api.route('/machines/<int:machineid>')
def get_machine_by_id(machineid):
    machine = Machine.get_one_by_id(machineid)
    return jsonify({'machine': machine})
# ______________________________________
