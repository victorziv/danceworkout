from flask import render_template
from flask import current_app as cap
from . import main
# ______________________________


@main.route('/', methods=['GET'])
@main.route('/home', methods=['GET'])
def home():
    return render_template(
        "main/home.html",
        config=cap.config,
    )
# ______________________________


@main.route('/contact', methods=['GET'])
def contact():
    return render_template(
        "main/contact.html",
        config=cap.config,
    )
# ______________________________


@main.route('/details', methods=['GET'])
def details():
    return render_template(
        "main/details.html",
        config=cap.config,
    )
# ______________________________
