var zebra;
// __________________________________


function notifyError(msg,onCloseCallback, auto_close) {
    auto_close = (typeof auto_close === "undefined") ? 3000 : auto_close;
    new jQuery.Zebra_Dialog( msg,
        {
            buttons:  false,
            modal: false,
            type : 'error',
            title : '<strong>Error!</strong>',
            position: ['right - 20', 'top + 70'],
            onClose: onCloseCallback,
            auto_close : auto_close,
        }
    );
}
//            custom_class : 'custom_error' 
// __________________________________

function notifySuccess(msg,onCloseCallback, auto_close) {
    auto_close = (typeof auto_close === "undefined") ? 3000 : auto_close;
    new jQuery.Zebra_Dialog( msg,
        {
            buttons:  false,
            modal: false,
            type : 'confirmation',
            title : '<strong>Success!</strong>',
            position: ['right - 20', 'top + 70'],
            onClose: onCloseCallback,
            auto_close : auto_close,
        }
    );
}
//            custom_class : 'custom_error' 
// __________________________________

function startWait(msg) {

    if (! msg) {
        msg = "Please wait...";
    }

    zebra = new jQuery.Zebra_Dialog( msg,
        {
            buttons:  false,
            modal: true,
            type : 'information',
            title : false,
            position: ['right - 20', 'top + 70'],
            show_close_button: false,
            custom_class : 'custom_zebra_class' 
        }
    );
}
// __________________________________

function stopWait() {
    zebra.close();
}

// __________________________________
