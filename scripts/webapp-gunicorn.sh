#!/bin/bash

NAME=danceworkout
curdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECTDIR=$(dirname ${curdir})
SOCKFILE=${PROJECTDIR}/run/${NAME}.sock
USER=dancew11
GROUP=dancew11
NUM_WORKERS=2

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
source ${PROJECTDIR}/venv/bin/activate

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

$(which gunicorn) manage:app --name ${NAME} --workers ${NUM_WORKERS} --user ${USER} --bind=unix:${SOCKFILE}

